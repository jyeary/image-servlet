/*
 * Copyright 2013 Blue Lotus Software, LLC..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluelotussoftware.servlet;

import com.gargoylesoftware.htmlunit.UnexpectedPage;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class ImageServletTest {

    private WebClient webClient;

    public ImageServletTest() {
    }

    @Before
    public void setUp() {
        webClient = new WebClient();
    }

    @Test
    public void statusCode() throws IOException {
        final UnexpectedPage page = webClient.getPage("http://localhost:8080/test/images/woodstock.jpg");
        final WebResponse response = page.getWebResponse();
        assertEquals(200, response.getStatusCode());
    }

    @Test
    public void contentDisposition() throws IOException {
        final UnexpectedPage page = webClient.getPage("http://localhost:8080/test/images/woodstock.jpg");
        final WebResponse response = page.getWebResponse();
        assertEquals("inline; filename=\"woodstock.jpg\"", response.getResponseHeaderValue("Content-Disposition"));
        webClient.closeAllWindows();
    }

    @Test
    public void contentType() throws IOException {
        final UnexpectedPage page = webClient.getPage("http://localhost:8080/test/images/woodstock.jpg");
        final WebResponse response = page.getWebResponse();
        assertEquals("image/jpeg", response.getContentType());
        webClient.closeAllWindows();
    }

    @Test
    public void contentLength() throws IOException {
        final UnexpectedPage page = webClient.getPage("http://localhost:8080/test/images/woodstock.jpg");
        final WebResponse response = page.getWebResponse();
        assertEquals(8907, Integer.parseInt(response.getResponseHeaderValue("Content-Length")));
        webClient.closeAllWindows();
    }

    @Test
    public void image() throws IOException {
        final UnexpectedPage page = webClient.getPage("http://localhost:8080/test/images/woodstock.jpg");
        InputStream is = page.getInputStream();
        BufferedImage bi = convertInputStreamToBufferedImage(is);
        assertNotNull(bi);
        assertEquals(263, bi.getHeight());
        assertEquals(192, bi.getWidth());
        assertEquals(BufferedImage.TYPE_3BYTE_BGR, bi.getType());
        webClient.closeAllWindows();
    }

    @Test
    public void badRequest() throws IOException {
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setPrintContentOnFailingStatusCode(false);
        final HtmlPage page = webClient.getPage("http://localhost:8080/test/images/");
        final WebResponse response = page.getWebResponse();
        assertEquals(400, response.getStatusCode());
        assertEquals("An image name is required.", response.getStatusMessage());
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(true);
        webClient.getOptions().setPrintContentOnFailingStatusCode(true);
        webClient.closeAllWindows();
    }

    @Test
    public void notFound() throws IOException {
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setPrintContentOnFailingStatusCode(false);
        final HtmlPage page = webClient.getPage("http://localhost:8080/test/images/abc.png");
        final WebResponse response = page.getWebResponse();
        assertEquals(404, response.getStatusCode());
        assertEquals("abc.png was not found.", response.getStatusMessage());
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(true);
        webClient.getOptions().setPrintContentOnFailingStatusCode(true);
        webClient.closeAllWindows();
    }

    public BufferedImage convertInputStreamToBufferedImage(final InputStream is) throws IOException {
        BufferedImage image = null;
        if (is != null) {
            image = ImageIO.read(is);
        }
        return image;
    }
}
