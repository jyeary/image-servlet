/**
 * This work by Bauke Sholtz and John Yeary is licensed under a Creative Commons
 * Attribution 3.0 Unported License.
 *
 * http://creativecommons.org/licenses/by-sa/3.0/legalcode
 *
 */
package com.bluelotussoftware.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>This image servlet is based primarily on the work of Bauke Scholtz
 * (BalusC) from a stackoverflow post <a
 * href="http://stackoverflow.com/a/1812356/160361">Simplest way to serve static
 * data from outside the application server in a Java web application</a> </p>
 * <p>The unit testing was added to help answer another question on
 * stackoverflow <a href="http://stackoverflow.com/q/90907/160361">Unit testing
 * a java servlet</a></p> <p>The servlet works very well at displaying inline
 * images for use in &lt;img src=&quot;/images/woodstock.jpg&quot;/&gt;, or with
 * JSF &lt;h:graphicImage value=&quot;/images/woodstock.jpg&quot;/&gt;. </p>
 *
 * @author Bauke Scholtz
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@WebServlet(name = "ImageServlet", urlPatterns = {"/images/*"}, initParams = {
    @WebInitParam(name = "basePath", value = "/resources/images")
})
public class ImageServlet extends HttpServlet {

    private static final long serialVersionUID = -6766956910587272711L;
    public static final String INFO = "com.bluelotussoftware.servlet.ImageServlet/1.0";
    public static final String DEFAULT_IMAGE_PATH = "/resources/images";

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String basePath = getServletContext().getRealPath(getInitParameter("basePath"));

        if (basePath == null) {
            basePath = getServletContext().getRealPath(DEFAULT_IMAGE_PATH);
        }

        if (request.getPathInfo() == null || "/".equals(request.getPathInfo())) {
            //NOI18N
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "An image name is required.");
            return;
        }

        String filename = URLDecoder.decode(request.getPathInfo(), "UTF-8");
        File file = new File(basePath, filename);

        if (!file.exists()) {
            //NOI18N
            response.sendError(HttpServletResponse.SC_NOT_FOUND, file.getName() + " was not found.");
            return;
        }

        response.setContentType(getServletContext().getMimeType(file.getName()));
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");

        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            input = new BufferedInputStream(new FileInputStream(file));
            output = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[8192];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ignore) {
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return INFO;
    }// </editor-fold>
}
